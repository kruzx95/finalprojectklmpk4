<!-- resources/views/makanan.blade.php -->

@extends('layouts.app')

@section('content')
    <h1>Daftar Makanan</h1>
    
    @foreach ($makanan as $item)
        <div>
            <h3>{{ $item->nama_makanan }}</h3>
            <p>Harga: {{ $item->harga }}</p>
            <p>Kategori: {{ $item->kategori->nama_kategori }}</p>
            <p>{{ $item->deskripsi }}</p>
        </div>
        <hr>
    @endforeach
@endsection
