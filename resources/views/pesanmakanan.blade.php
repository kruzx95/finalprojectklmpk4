<!-- resources/views/pesanmakanan.blade.php -->

@extends('layouts.app')

@section('content')
    <h1>Pesan Makanan</h1>

    <form action="/pesan-makanan" method="post">
        @csrf
        <label for="user_id">User ID:</label>
        <input type="text" name="user_id" required>
        
        <label for="makanan_id">Makanan ID:</label>
        <input type="text" name="makanan_id" required>
        
        <label for="jumlah">Jumlah:</label>
        <input type="text" name="jumlah" required>
        
        <label for="total_harga">Total Harga:</label>
        <input type="text" name="total_harga" required>
        
        <label for="status_pesanan">Status Pesanan:</label>
        <input type="text" name="status_pesanan" required>

        <button type="submit">Pesan Makanan</button>
    </form>
@endsection
