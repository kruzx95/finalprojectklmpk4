<!-- resources/views/kategori.blade.php -->

@extends('layouts.app')

@section('content')
    <h1>Daftar Kategori</h1>
    
    @foreach ($kategori as $item)
        <div>
            <h3>{{ $item->nama_kategori }}</h3>
        </div>
        <hr>
    @endforeach
@endsection
